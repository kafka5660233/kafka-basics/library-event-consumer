package com.learnkafka.consume.libraryeventconsumer.consumer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafka.consume.libraryeventconsumer.consumer.LibraryEventConsumer;
import com.learnkafka.consume.libraryeventconsumer.domain.Book;
import com.learnkafka.consume.libraryeventconsumer.domain.LibraryEvent;
import com.learnkafka.consume.libraryeventconsumer.domain.LibraryEventType;
import com.learnkafka.consume.libraryeventconsumer.kakacustomrecord.MyCustomConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@EmbeddedKafka(
        topics = "${spring.kafka.topic}",
        partitions = 1
)
class LibraryEventConsumerIntegrationTest {

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    @Autowired
    private KafkaTemplate<Integer, String> kafkaTemplate;

    @Autowired
    private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

    @Value(value = "${spring.kafka.topic}")
    private String topic;

    @SpyBean
    private LibraryEventConsumer libraryEventConsumer;

    @BeforeEach
    void setUp() {
        for(MessageListenerContainer messageListenerContainer : kafkaListenerEndpointRegistry.getListenerContainers()){
            ContainerTestUtils.waitForAssignment(messageListenerContainer, embeddedKafkaBroker.getPartitionsPerTopic());
        }
    }

    @AfterEach
    void tearDown() {
    }

    @Disabled // FIXME : re visit the tutorial and fix it
    @Test
    void onMessage() throws Exception {
        // given
        LibraryEvent request = new LibraryEvent(
                null,
                LibraryEventType.NEW,
                new Book(456, "int-test-book","int-test-author")
        );
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonRequest = objectMapper.writeValueAsString(request);
        kafkaTemplate.send(topic, jsonRequest).get();
        // when
        CountDownLatch countDownLatch = new CountDownLatch(1);
        countDownLatch.await(3, TimeUnit.SECONDS);

        // then
        Mockito
                .verify(libraryEventConsumer, Mockito.times(1))
                .onMessage(Mockito.any(MyCustomConsumerRecord.class), Mockito.any(Map.class));

    }
}
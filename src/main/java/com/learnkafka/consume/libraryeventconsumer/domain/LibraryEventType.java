package com.learnkafka.consume.libraryeventconsumer.domain;

public enum LibraryEventType {
    NEW,
    UPDATE
    ;
}

package com.learnkafka.consume.libraryeventconsumer.kakacustomrecord;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.record.TimestampType;

import java.util.Optional;

public class MyCustomConsumerRecord<K,V> extends ConsumerRecord<K, V> {

    public MyCustomConsumerRecord(String topic, int partition, long offset, long timestamp, TimestampType timestampType, int serializedKeySize, int serializedValueSize, K key, V value, Headers headers, Optional<Integer> leaderEpoch) {
        super(topic, partition, offset, timestamp, timestampType, serializedKeySize, serializedValueSize, key, value, headers, leaderEpoch);
    }

    private long startTS;

    private long endTS;

    public long getStartTS() {
        return startTS;
    }

    public void setStartTS(long startTS) {
        this.startTS = startTS;
    }

    public long getEndTS() {
        return endTS;
    }

    public void setEndTS(long endTS) {
        this.endTS = endTS;
    }

    public static <K, V> MyCustomConsumerRecord<K, V> from(ConsumerRecord<K, V> record) {
        return new MyCustomConsumerRecord<>(
                record.topic(),
                record.partition(),
                record.offset(),
                record.timestamp(),
                record.timestampType(),
                record.serializedKeySize(),
                record.serializedValueSize(),
                record.key(),
                record.value(),
                record.headers(),
                record.leaderEpoch()
        );
    }
}

package com.learnkafka.consume.libraryeventconsumer.config;

import com.learnkafka.consume.libraryeventconsumer.interceptor.MyFirstRecordInterceptor;
import com.learnkafka.consume.libraryeventconsumer.interceptor.MySecondRecordInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.*;
import org.springframework.kafka.support.ExponentialBackOffWithMaxRetries;
import org.springframework.util.backoff.ExponentialBackOff;

import java.util.stream.Stream;

@Configuration
@EnableKafka
@Slf4j
public class LibraryEventConsumerConfig {

    @Value("${topics.retry}")
    private String retryTopic;

    @Value("${topics.dlt}")
    private String deadLetterTopic;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Bean
    public CommonErrorHandler errorHandler() {
        // BackOff backOff = new FixedBackOff(10_000, 5);
        ExponentialBackOff backOff = new ExponentialBackOffWithMaxRetries(3);
        backOff.setInitialInterval(2_000L);
        backOff.setMultiplier(2.0); //  2_000 -> 2*2_000  -> 2*2*2_000 -> .. till allowed maxRetries
        backOff.setMaxInterval(13_000L); // constant after interval* multiplier > maxInterval
        /*
        WORKING
        - errorHandler WITHOUT RECOVERY
         */
        // DefaultErrorHandler errorHandler = new DefaultErrorHandler(backOff);
        /*
        WORKING
        - errorHandler WITH RECOVERY
         */
        DefaultErrorHandler errorHandler = new DefaultErrorHandler(publishingRecoverer(), backOff);

        // config to ignore these exception from retry - start
        Stream.of(NullPointerException.class)
                .forEach(errorHandler::addNotRetryableExceptions);
        // config to ignore these exception from retry - end

        // config to add retry listener - start
        errorHandler.setRetryListeners(getRetryListener());
        // config to add recovery listener - end
        return errorHandler;
    }

    @Bean
    public <K, V> RecordInterceptor<K, V> recordInterceptor() {
        return new CompositeRecordInterceptor<>(
                new MyFirstRecordInterceptor<>(),
                new MySecondRecordInterceptor<>()
        );
    }

    private RetryListener getRetryListener() {
        return (record, ex, deliveryAttempt) -> {
            log.error("""
                    Processing the msg failed
                    record : {}
                    exception : {}
                    deliveryAttempt : {}
                    """, record.value(), ex.getCause().getMessage(), deliveryAttempt);
        };
    }

    private DeadLetterPublishingRecoverer publishingRecoverer() {
        DeadLetterPublishingRecoverer recoverer = new DeadLetterPublishingRecoverer(kafkaTemplate, (r, e) -> {
            log.error("Exception in publishingRecoverer : {} ", e.getCause().getMessage(), e);
            if (!(e.getCause() instanceof NullPointerException)) {
                return new TopicPartition(retryTopic, r.partition());
            } else {
                return new TopicPartition(deadLetterTopic, r.partition());
            }
        });
        return recoverer;
    }
}

package com.learnkafka.consume.libraryeventconsumer.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class LibraryEventRetryConsumer {
    @KafkaListener(
            topics = {"#{'${topics.retry}'.split(',')}" },
            groupId = "library-event-consumer-group-RETRY"
    )
    public void onMessage(ConsumerRecord<String, String> record) throws Exception {
        log.info("Consumer record from RETRY consumer : {} ", record);
        if(record.value().startsWith("e")){
            throw new Exception("msgggggggggggg startssssssssss withhhhh error - e");
        } else if(record.value().startsWith("npe")){
            throw new NullPointerException("msgggggggggggg startssssssssss withhhhh error - npe");
        } else if(record.value().startsWith("rte")){
            throw new RuntimeException("msgggggggggggg startssssssssss withhhhh error - rte");
        }
    }
}

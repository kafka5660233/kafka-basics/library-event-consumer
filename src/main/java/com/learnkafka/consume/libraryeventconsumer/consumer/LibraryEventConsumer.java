package com.learnkafka.consume.libraryeventconsumer.consumer;

import com.learnkafka.consume.libraryeventconsumer.kakacustomrecord.MyCustomConsumerRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class LibraryEventConsumer {

    @KafkaListener(
            topics = {"#{'${spring.kafka.topic}'.split(',')}" },
            groupId = "library-event-consumer-group"
    )
    public void onMessage(
            @Payload MyCustomConsumerRecord<String, String> record,
            @Headers Map<String, Object> headers) throws Exception {
        log.info("Consumer record : {} ", record);
        log.info("Headers : {} ", headers);
        record.setStartTS(System.currentTimeMillis());
        if(record.value().startsWith("e")){
            throw new Exception("msgggggggggggg startssssssssss withhhhh error - e");
        } else if(record.value().startsWith("npe")){
            throw new NullPointerException("msgggggggggggg startssssssssss withhhhh error - npe");
        } else if(record.value().startsWith("rte")){
            throw new RuntimeException("msgggggggggggg startssssssssss withhhhh error - rte");
        }
        Thread.sleep(1_000L);
        record.setEndTS(System.currentTimeMillis());
        log.info("""
                
                record.getStartTS() : {}
                record.getEndTS() : {}
                """, record.getStartTS(), record.getEndTS());
    }
}

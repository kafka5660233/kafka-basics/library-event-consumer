package com.learnkafka.consume.libraryeventconsumer.interceptor;

import com.learnkafka.consume.libraryeventconsumer.kakacustomrecord.MyCustomConsumerRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.RecordInterceptor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MySecondRecordInterceptor<K, V> implements RecordInterceptor<K,V> {
    @Override
    public ConsumerRecord<K, V> intercept(ConsumerRecord<K, V> record, Consumer<K, V> consumer) {
        log.info("""
                MySecondRecordInterceptor -> intercptttttttttttttttttttttttttttttttttttttttttttttttttt
                """);
        return record;
    }

    @Override
    public void success(ConsumerRecord<K, V> record, Consumer<K, V> consumer) {
        log.info("""
                MySecondRecordInterceptor -> successssssssssssssssssssssssssssssssssssssssssssssssssss
                """);
        RecordInterceptor.super.success(record, consumer);
    }

    @Override
    public void failure(ConsumerRecord<K, V> record, Exception exception, Consumer<K, V> consumer) {
        log.info("""
                MySecondRecordInterceptor -> failureeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
                """);
        RecordInterceptor.super.failure(record, exception, consumer);
    }

    @Override
    public void afterRecord(ConsumerRecord<K, V> record, Consumer<K, V> consumer) {
        log.info("""
                MySecondRecordInterceptor -> afterRecordddddddddddddddddddddddddddddddddddddddddddddd
                """);
        RecordInterceptor.super.afterRecord(record, consumer);
        log.info("""
                
                record.getStartTS() : {}
                record.getEndTS() : {}
                """, ((MyCustomConsumerRecord)record).getStartTS(), ((MyCustomConsumerRecord)record).getEndTS());
    }
}

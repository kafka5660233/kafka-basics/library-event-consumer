package com.learnkafka.consume.libraryeventconsumer.interceptor;

import com.learnkafka.consume.libraryeventconsumer.kakacustomrecord.MyCustomConsumerRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.RecordInterceptor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MyFirstRecordInterceptor<K, V> implements RecordInterceptor<K,V> {
    @Override
    public ConsumerRecord<K, V> intercept(ConsumerRecord<K, V> record, Consumer<K, V> consumer) {
        log.info("""
                MyFirstRecordInterceptor -> intercptttttttttttttttttttttttttttttttttttttttttttttttttt
                """);
        MyCustomConsumerRecord<K, V> myRecord = MyCustomConsumerRecord.from(record);
        return myRecord;
    }

    @Override
    public void success(ConsumerRecord<K, V> record, Consumer<K, V> consumer) {
        log.info("""
                MyFirstRecordInterceptor -> successssssssssssssssssssssssssssssssssssssssssssssssssss
                """);
        RecordInterceptor.super.success(record, consumer);
    }

    @Override
    public void failure(ConsumerRecord<K, V> record, Exception exception, Consumer<K, V> consumer) {
        log.info("""
                MyFirstRecordInterceptor -> failureeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
                """);
        RecordInterceptor.super.failure(record, exception, consumer);
    }

    @Override
    public void afterRecord(ConsumerRecord<K, V> record, Consumer<K, V> consumer) {
        log.info("""
                MyFirstRecordInterceptor -> afterRecordddddddddddddddddddddddddddddddddddddddddddddd
                """);
        RecordInterceptor.super.afterRecord(record, consumer);
        log.info("""
                
                record.getStartTS() : {}
                record.getEndTS() : {}
                """, ((MyCustomConsumerRecord)record).getStartTS(), ((MyCustomConsumerRecord)record).getEndTS());
    }
}

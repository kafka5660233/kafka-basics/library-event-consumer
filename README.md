# Library Event Subscriber/Consumer/Listener

```
Platform - Udemy
Cource name -  Apache Kafka for Developers using Spring Boot[LatestEdition] 
Instrutor - Pragmatic Code School
href - https://www.udemy.com/course/apache-kafka-for-developers-using-springboot 
```

### DependsOn
```
This project depend on the kafka producer
Gitlab 
href - https://gitlab.com/kafka5660233/kafka-basics/library-event-producer
clone url -  https://gitlab.com/kafka5660233/kafka-basics/library-event-producer.git

```

### Tests

```
This project has
**integration tests**

```

### Notes
```
Interceptors :
  -- ref MyFirstRecordInterceptor
  -- ref MySecondRecordInterceptor

Configuring Interceptors
  -- ref  LibraryEventConsumerConfig.recordInterceptor()

Custom ConsumerRecord
  -- ref MyCustomConsumerRecord // used in LibraryEventConsumer
```
